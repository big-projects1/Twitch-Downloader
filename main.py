from twitchBandit import twitchBandit
from selenium import webdriver

def main():
    options = webdriver.ChromeOptions()
    options.add_argument("start-maximized")
    options.add_argument("window-size=1980,1030")
    options.add_argument("headless")
    options.add_argument("log-level=3")
    driver = webdriver.Chrome(options=options)


    twitchBandit().run(driver,r'https://www.twitch.tv/directory/game/League%20of%20Legends/clips?range=24hr')
    

if __name__ == "__main__":
    main()