from urllib.request import urlretrieve
from selenium.webdriver.common.by import By
from time import sleep
import subprocess
import os
import emoji
import cv2
from selenium.webdriver.common.keys import Keys

def is_emoji(s):
    return len(emoji.distinct_emoji_list(s)) == 1


def deleteFiles():
    cnt = 0
    for file in os.listdir("./"):
       filename = os.fsdecode(file)
       if filename.endswith(".mkv") or filename.endswith(".txt") or filename.endswith(".mp3"):
           os.remove(file)
           cnt+=1
    for file in os.listdir("./fixed"):
       filename = os.fsdecode(file)
       if filename.endswith(".mkv") or filename.endswith(".txt") or filename.endswith(".mp3"):
           os.remove(f"./fixed/{file}")
           cnt+=1
    print(f"{cnt} temporary files removed")



def catchRussians(string):
    russianCheck = False
    alphaChars = 0
    for letter in string:
        if(letter.isalpha()):
            alphaChars += 1
        russianCheck = not(letter.isascii()) and not(is_emoji(letter))
        
        if(russianCheck):
            break
    
    if alphaChars > 1 and not russianCheck:
        return False
    else:
        return True
    
def formatVideosAndSavePaths(clips):
    with open("path.txt","w+") as f:
        for i in clips:
            subprocess.call(f'ffmpeg -i "{i}" -c:v libx265 -b:v 8M -minrate 8M -preset medium -vf scale=1920:1080 -c:a aac -b:a 320K "./fixed/{i[:-4]}_fix.mkv"')
            f.write(f"file './fixed/{i[:-4]}_fix.mkv'\n")

    
FINAL_VIDEO_TITLE = ""
CLIP_TITLES = []
CREATOR_TITLES = []
CLIP_COUNT = 204


class twitchBandit():
    def __init__(self) -> None:
        pass

    def run(self,driver,link,delete=True):

        if delete : deleteFiles()

        driver.get(link)
        sleep(5) 

        #Scroll the screen to get more videos
        #TODO: Fix it it dont work
        card = driver.find_element(By.XPATH,'//a[@data-a-target="preview-card-image-link"]')
        card.send_keys(Keys.END) # Add a while loop to do infinite scroll

        sleep(5)
        videoBoxes = driver.find_elements(By.XPATH, '//a[@data-a-target="preview-card-image-link"]')

        #Get all video pages, get video titles, creator titles and filter content
        #TODO: More filtering
        links =[]
        for i,box in enumerate(videoBoxes[:CLIP_COUNT]):
            url = box.get_attribute("href")
            
            clip_title = box.find_element(By.XPATH,".//img").get_attribute("alt")
            creator_title = url.split("/")[3]

            if catchRussians(clip_title):
                print(f"{i+1}/{CLIP_COUNT} -- {clip_title} by {creator_title} not english - aborting")
                continue
            
            clip_title = ''.join([i if ord(i) < 128 else ' ' for i in clip_title])
            CLIP_TITLES.append(clip_title.replace("/", "").replace(":"," -"))
            CREATOR_TITLES.append(creator_title.replace("/", " "))
            links.append(url)
            print(f"{i+1}/{CLIP_COUNT} -- {clip_title} by {creator_title} found")

        print("\n----------------------------------\n")

        for i,link in enumerate(links):
            driver.get(link)
            sleep(2)
            el = driver.find_elements(By.XPATH, '//video')
            url = el[0].get_attribute("src")

            if(url==""): continue

            try:
                urlretrieve(url, f'{CLIP_TITLES[i]}.mkv')
                print(f"{i+1}/{len(links)} -- {CLIP_TITLES[i]} by {CREATOR_TITLES[i]} downloaded")
            except Exception as ex:
                print(f"{i+1}/{len(links)} -- {CLIP_TITLES[i]} by {CREATOR_TITLES[i]} failed to download.. retrying")
                print(ex)

        print("\n----------------------------------\n")
        clips = []

        for i,file in enumerate(os.listdir("./")):
                filename = os.fsdecode(file)


                if filename.endswith(".mkv"):
                    vid = cv2.VideoCapture(file)
                    height = int(vid.get(cv2.CAP_PROP_FRAME_HEIGHT))
                    if(height!=1080):
                        print("File resolution missmatch - abort")
                        continue
                    
                    print(filename+" joining") #do your video process here
                    clips.append(filename)
                else:
                    continue

        formatVideosAndSavePaths(clips)

        FINAL_VIDEO_TITLE = CLIP_TITLES[0]
        ffmpeg_prompt = f'ffmpeg -f concat -safe 0 -segment_time_metadata 1 -i path.txt -vf select=concatdec_select -af aselect=concatdec_select,aresample=async=1 -max_interleave_delta 0 "./finals/{FINAL_VIDEO_TITLE}.mkv"'
        subprocess.call(ffmpeg_prompt,shell=True)
        driver.close()



